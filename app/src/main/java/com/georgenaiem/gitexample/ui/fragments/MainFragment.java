package com.georgenaiem.gitexample.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.georgenaiem.gitexample.R;
import com.georgenaiem.gitexample.managers.APIManager;
import com.georgenaiem.gitexample.models.Contributor;
import com.georgenaiem.gitexample.ui.adapters.ContributorsAdapter;
import com.georgenaiem.gitexample.ui.common.PaginationScrollListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment implements Callback<List<Contributor>>, ContributorsAdapter.Listener {

    @BindView(R.id.error_textView)
    TextView errorTextView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.content_main)
    RelativeLayout contentMain;
    Unbinder unbinder;

    private APIManager apiManager;
    private ContributorsAdapter mAdapter;

    private int pagesCount = 1;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    public MainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        apiManager = APIManager.retrofit.create(APIManager.class);
        loadContributors(pagesCount);
        return rootView;
    }

    private void loadContributors(int page) {
        apiManager.contributors("ruby", "ruby", page).enqueue(this);
        isLoading = true;
        Log.d("Loading", "page: " + page);
    }

    private void setContributors(List<Contributor> contributors) {
        if (recyclerView == null) {
            return;
        }

        if (mAdapter == null && contributors.size() == 0) {
            showError();
            return;
        }

        if (contributors.size() == 0) {
            isLastPage = true;
            return;
        }

        if (mAdapter == null) {
            initializeAdapter();
        }

        mAdapter.add(contributors);
        pagesCount++;
    }

    private void initializeAdapter() {
        mAdapter = new ContributorsAdapter(new ArrayList<Contributor>());
        mAdapter.mCallback = this;
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new PaginationScrollListener(llm) {
            @Override
            protected void loadMoreItems() {
                loadContributors(pagesCount);
            }

            @Override
            public int getTotalPageCount() {
                return pagesCount;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void showError() {

        if (mAdapter == null || mAdapter.getItemCount() == 0) {
            errorTextView.setVisibility(View.VISIBLE);
            errorTextView.setText("Unable to connect\nclick to retry");
        } else {
            Snackbar mSnackbar = Snackbar.make(getView(), "Unable to connect", Snackbar.LENGTH_LONG)
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!isLoading)
                                loadContributors(pagesCount);
                        }
                    });
            mSnackbar.show();
        }
    }

    @OnClick(R.id.error_textView)
    public void retry(View v) {
        errorTextView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        loadContributors(pagesCount);

    }

    @Override
    public void onResponse(@NonNull Call<List<Contributor>> call, @NonNull Response<List<Contributor>> response) {
        progressBar.setVisibility(View.GONE);
        errorTextView.setVisibility(View.GONE);
        isLoading = false;
        if (response.isSuccessful()) {
            if (response.body() != null)
                setContributors(response.body());
            else
                isLastPage = true;
        } else {
            showError();
        }
    }

    @Override
    public void onFailure(@NonNull Call<List<Contributor>> call, @NonNull Throwable t) {
        progressBar.setVisibility(View.GONE);
        isLoading = false;
        showError();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(Contributor contributor) {
        // opens details fragment
    }
}
