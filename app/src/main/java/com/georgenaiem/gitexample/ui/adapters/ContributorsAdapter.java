package com.georgenaiem.gitexample.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.georgenaiem.gitexample.R;
import com.georgenaiem.gitexample.models.Contributor;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContributorsAdapter extends RecyclerView.Adapter<ContributorsAdapter.ViewHolder> {

    public Listener mCallback;
    private List<Contributor> mDataSet;

    public ContributorsAdapter(List<Contributor> mDataSet) {
        this.mDataSet = mDataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater
                .from(parent.getContext()).inflate(R.layout.index_contributor, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Contributor item = mDataSet.get(position);

        Glide.with(holder.avatarImageView.getContext())
                .load(item.avatarUrl)
                .apply(new RequestOptions().circleCrop())
                .into(holder.avatarImageView);

        holder.nameTextView.setText(item.login);
        holder.commitsTextView.setText(String.format(Locale.getDefault(), "%d %s", item.contributions, "commits"));
    }


    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public void add(List<Contributor> contributors) {
        mDataSet.addAll(contributors);
        notifyDataSetChanged();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avatar_imageView)
        ImageView avatarImageView;
        @BindView(R.id.name_textView)
        TextView nameTextView;
        @BindView(R.id.commits_textView)
        TextView commitsTextView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.contributor_view)
        public void onClick(View v) {
            if (mCallback != null)
                mCallback.onClick(mDataSet.get(getAdapterPosition()));
        }
    }

    public interface Listener {
        void onClick(Contributor contributor);
    }
}
